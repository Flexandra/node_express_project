const express = require('express');
const path = require('path');
const favicon = require('static-favicon');
const logger = require('morgan');
//var createError = require('http-errors');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');

//Локаль
const i18n=require("i18n-express"); // <-- require the module

//Теперь мы используем эту конфигурацию в app.js и подключаемся к базе данных при помощи Mongoose API:
//var dbConfig = require('db.js');
const dbConfig = require('./db');
const mongoose = require('mongoose');
// Connect to DB
mongoose.connect(dbConfig.url);

const db = mongoose.connect(dbConfig.url);
const app = express();


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');


app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


// Configuring Passport
const passport = require('passport');
const expressSession = require('express-session');
// Why Do we need this key ?
//app.use(expressSession({secret: 'mySecretKey'}));

app.use(expressSession({
	secret: 'mySecretKey',
	resave: true,
	saveUninitialized: true

}));

app.use(passport.initialize());
app.use(passport.session());

//Локаль
app.use(i18n({
  translationsPath: path.join(__dirname, 'i18n'), // <--- use here. Specify translations files path.
  siteLangs: ["en","ru"],
  defaultLang: "ru",
  textsVarName: 'translation'
}));


// Using the flash middleware provided by connect-flash to store messages in session
 // and displaying in templates
const flash = require('connect-flash');
app.use(flash());


// Initialize Passport
const initPassport = require('./passport/init');
initPassport(passport);

const RouteRegister = require('./routes/RouteRegister');
RouteRegister(app);


/// catch 404 and forward to error handler
app.use(function(req, res, next) {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
});


// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
 }
 
 
 module.exports = app;