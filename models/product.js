var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var myproducts = new Schema({
    	productName: String,
		proteins: Number,
		fats: Number,
		carbohydrates: Number,
		calories: Number   	
		});	
		
module.exports = mongoose.model('products', myproducts);