var mongoose = require('mongoose');

//создаем модель Mongoose, при помощи которой мы можем выполнять операции CRUD над данными базы, лежащей в основе
module.exports = mongoose.model('User',{
	id: String,
	username: String,
	password: String,
	email: String,
	firstName: String,
	lastName: String
});