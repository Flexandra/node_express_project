var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var dietSchema = new Schema({
    	userId: Schema.ObjectId,
    	productName: String,
    	weight: Number  	
		});	
		
module.exports = mongoose.model('Diet', dietSchema);