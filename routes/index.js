const express = require('express');
const router = express.Router();

router.get('/', function (req, res) {
    // Display the Login page with any flash message, if any
    res.render('index', {message: req.flash('message')});
});

module.exports = router;