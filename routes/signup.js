const express = require('express');
const router = express.Router();
const passport = require('passport');

/* GET Registration Page */
router.get('/signup', function (req, res) {
    res.render('register', {message: req.flash('message')});
});

/* Handle Registration POST */
router.post('/signup', passport.authenticate('signup', {
    successRedirect: '/home',
    failureRedirect: '/signup',
    failureFlash: true
}));

module.exports = router;