const express = require('express');
const router = express.Router();
const products = require('./../models/product');
const diets = require('./../models/diet');
const showmass = require('./../models/findmass')(products);

const isAuthenticated = function (req, res, next) {
    // if user is authenticated in the session, call the next() to call the next request handler
    // Passport adds this method to request object. A middleware is allowed to add properties to
    // request and response objects
    if (req.isAuthenticated())
        return next();
    // if the user is not authenticated then redirect him to the login page
    res.redirect('/');
};

/* Подготовить список в нужном формате из продуктов */
const getProductsContent = (allproductList) => {
    let productContent = [];

    for (let i = 0; i < allproductList.length; i++) {
        //Соберем из базы массив, содаржащий все продукты с их характеристиками
        productContent[i] = {
            "productName": allproductList[i]['productName'],
            "proteins": allproductList[i]['proteins'],
            "fats": allproductList[i]['fats'],
            "carbohydrates": allproductList[i]['carbohydrates'],
            "calories": allproductList[i]['calories']
        };
    }

    return productContent;
};

/** Подготовить данные для формирования таблицы */
const buildUserTableData = (userTable, allproductList) => {
//Переменные для итоговой строки таблицы с диетой
    let totalWeight = 0;
    let totalProteins = 0;
    let totalFats = 0;
    let totalCarbohydrates = 0;
    let totalCalories = 0;

    for (let i = 0; i < userTable.length; i++) {
        //console.log('ПОКАЖИ userTable.length', userTable.length);
        //console.log('ПОКАЖИ i', i);
        //console.log('ПОКАЖИ userTable[i]', userTable[i]);

        let obj = userTable[i];
        //console.log('ПОКАЖИ obj', obj);

        let pointProduct = '';
        let pointWeight = 0;
        let productContent = getProductsContent(allproductList);

        for (let prop in obj) {
            //console.log("obj." + prop + " = " + obj[prop]);

            if (prop === 'userProduct') {
                pointProduct = obj[prop];
            }
            pointWeight = obj[prop];
        }
        //console.log('pointProduct РАВНО', pointProduct);
        //console.log('pointWeight РАВНО', pointWeight);


        for (let j = 0; j < productContent.length; j++) {
            //console.log('ПОКАЖИ productContent.length', productContent.length);
            //console.log('УРА МЫ ПРОДУКТ В БАЗЕ И ГОТОВЫ ПОКАЗАТЬ ПО НЕМУ ДАННЫЕ', productContent[i]['productName']);

            if (productContent[j]['productName'] != pointProduct) {
                continue;
            }

            //console.log('УРА МЫ НАШЛИ СОВПАДЕНИЕ В БАЗЕ с ПРОДУКТОМ ПОЛЬЗОВАТЕЛЯ  - ЭТО: ', productContent[j]['productName']);
            //userTable[i].proteins = productContent[j]['proteins'];
            userTable[i].proteins = parseFloat(((pointWeight * productContent[j]['proteins']) / 100).toFixed(2));
            totalProteins += userTable[i].proteins;

            userTable[i].fats = parseFloat(((pointWeight * productContent[j]['fats']) / 100).toFixed(2));
            totalFats += userTable[i].fats;

            userTable[i].carbohydrates = parseFloat(((pointWeight * productContent[j]['carbohydrates']) / 100).toFixed(2));
            totalCarbohydrates += userTable[i].carbohydrates;

            userTable[i].calories = parseFloat(((pointWeight * productContent[j]['calories']) / 100).toFixed(2));
            totalCalories += userTable[i].calories;

            totalWeight += pointWeight;
            //console.log('АБЛИЦА ПОЛЬЗОВАТЕЛЯ НА КАЖДОМ ШАГЕ (массив userTable): ', userTable);
        }
    }

    return {
        userTable: userTable,
        totalWeight: totalWeight,
        totalProteins: totalProteins.toFixed(2),
        totalFats: totalFats.toFixed(2),
        totalCarbohydrates: totalCarbohydrates.toFixed(2),
        totalCalories: totalCalories.toFixed(2)
    };
};

/** Добавить итоговый столбец */
const appendWithTotalResult = (userTable, usersTableData) => {
//Добавим в таблицу строку ИТОГО
    let lastItem = userTable.length;
    //console.log('ПОКАЖИ userTable.length (он же lastItem) ', lastItem);

    //СТРОКА ИТОГО
    userTable[lastItem] = {
        total: 'Итого',
        totalWeight: usersTableData.totalWeight,
        totalProteins: usersTableData.totalProteins,
        totalFats: usersTableData.totalFats,
        totalCarbohydrates: usersTableData.totalCarbohydrates,
        totalCalories: usersTableData.totalCalories
    };
};


/* GET Home Page */
router.get('/home', isAuthenticated, function (req, res) {
    //console.log('REQUEST SHOWMASS DATA', showmass);

    //Покажем ДИЕТУ ПОЛЬЗОВАТЕЛЯ
    let userId = req.user._id;

    //Создадим пустой массив для формирования таблицы с диетой пользователя
    //Массив с диетой пользователя
    let userTable = [];

    (async function () {
        try {
            let r = await diets.find().where('userId').equals(userId);
            //console.log('ПОКАЖИ ДИЕТУ ПОЛЬЗОВАТЕЛЯ: ', r);

            //Запишем в массив продукты, добавленные пользователем в свою таблицу
            for (let i = 0; i < r.length; i++) {
                userTable[i] = {"userProduct": r[i]['productName'], "weight": r[i]['weight']};
            }
            //console.log('ПОКАЖИ ТАБЛИЦУ ПОЛЬЗОВАТЕЛЯ (массив userTable): ', userTable);
            //console.log('ПОКАЖИ userTable[6]: ', userTable[6]);

            let allproductList = await products.find();
            //console.log('ПОКАЖИ allproductList', allproductList);

            let usersTableData = buildUserTableData(userTable, allproductList);
            userTable = usersTableData.userTable;

            //console.log('ТЕПЕРЬ внутри ТАБЛИЦА ПОЛЬЗОВАТЕЛЯ ВЫГЛЯДИТ ТАК (массив userTable): ', userTable);
            if (userTable.length > 0) {
                appendWithTotalResult(userTable, usersTableData);
            }

            showmass.then(function (data) {
                res.render('home', {user: req.user, showmass: data, userTable: userTable});
            });
        } catch (err) {
            console.log('НЕ ПОКАЖУ ДИЕТУ ПОЛЬЗОВАТЕЛЯ: ' + err);
        }
    })();
});



/* POST Home Page */
router.post('/home', isAuthenticated, function (req, res) {
    console.log('POST запрос прошел. УРА!');
    console.log('ЭТО req.body', req.body);

    let userId = req.user._id;
    let addedProductName = req.body.product;
    let addedProductWeight = req.body.weight;

    let deleteAll = '';


    //Переменная для хранения названия продукта, которого нет в базе
    let noProduct = '';

    //Создадим пустой массив для формирования таблицы с диетой пользователя
    //Массив с диетой пользователя
    let userTable = [];

    //Массив всех названий продуктов
    let productNames = [];


    deleteAll = req.body.dell;
    //console.log('ЭТО req.body.dell', req.body.dell);

    if (deleteAll) {
        //console.log('deleteAll НЕ пустая СТРОКА и равна: ', deleteAll);
        //код для удаления диеты пользователя из базы
        //Удалим продукт в список диеты пользователя
        diets
            .remove({"userId": userId})
            .then(() => {
                showmass.then(function (data) {
                    res.render('home', {
                        user: req.user,
                        product: addedProductName,
                        weight: addedProductWeight,
                        showmass: data,
                        userTable: userTable,
                        noProduct: noProduct
                    });
                })
                    .catch((err) => console.log("Show mass render error " + err));
            })
            .catch((err) => console.log('Error in Deleting: ' + err))
    } else {
        console.log('deleteAll ПУСТАЯ СТРОКА');
    }

    //Покажем ДИЕТУ ПОЛЬЗОВАТЕЛЯ
    userId = req.user._id;

    (async function () {
        try {
            let allproductList = await products.find();
            console.log('ПОКАЖИ allproductList', allproductList);

            if (addedProductName) {
                //Проверим, если ли в базе продукт с названием введенным пользователем
                for (let i = 0; i < allproductList.length; i++) {
                    productNames.push(allproductList[i]['productName']);
                }
                console.log('productNames ВЫГЛЯДИТ ТАК: ', productNames);

                //Переменная для хранения названия продукта, которого нет в базе
                //let noProduct = '';

                noProduct = productNames.indexOf(addedProductName);

                console.log('noProduct если ввести ПОМИДОР ВЫГЛЯДИТ ТАК: ', noProduct);
                console.log('addedProductName ВЫГЛЯДИТ ТАК: ', addedProductName);

                if (noProduct != -1) {
                    noProduct = '';

                    //Добавим продукт в список диеты пользователя
                    // Insert a single document
                    let addingProduct = await diets.create({
                        "userId": userId,
                        "productName": addedProductName,
                        "weight": addedProductWeight
                    });
                    console.log('Product saved succesful');
                } else {
                    noProduct = 'Нет данных по продукту ' + addedProductName;
                    console.log('userTable ЕСЛИ ДОБАВИЛИ НЕСУЩЕСТВУЮЩИЙ ПРОДУКТ выглядит так: ', userTable);
                }

                // Insert a single document
                let r = await diets.find().where('userId').equals(userId);
                //console.log('ПОКАЖИ ДИЕТУ ПОЛЬЗОВАТЕЛЯ: ', r);

                //Запишем в массив продукты, добавленные пользователем в свою таблицу
                for (let i = 0; i < r.length; i++) {
                    //userTable[i] = [r[i]['productName'], r[i]['weight']];
                    userTable[i] = {"userProduct": r[i]['productName'], "weight": r[i]['weight']};
                }
                //console.log('ПОКАЖИ ТАБЛИЦУ ПОЛЬЗОВАТЕЛЯ (массив userTable): ', userTable);
                //console.log('ПОКАЖИ userTable[6]: ', userTable[6]);
            }

            let usersTableData = buildUserTableData(userTable, allproductList);
            userTable = usersTableData.userTable;

            //console.log('ТЕПЕРЬ внутри ТАБЛИЦА ПОЛЬЗОВАТЕЛЯ ВЫГЛЯДИТ ТАК (массив userTable): ', userTable);

            //Добавим в таблицу строку ИТОГО
            if (userTable.length > 0) {
                appendWithTotalResult(userTable, usersTableData);
            }

            showmass.then(function (data) {
                res.render('home', {
                    user: req.user,
                    product: addedProductName,
                    weight: addedProductWeight,
                    showmass: data,
                    userTable: userTable,
                    noProduct: noProduct
                });
            })

            //console.log('ПОКАЖИ ДИЕТУ ПОЛЬЗОВАТЕЛЯ: ', r);
        } catch (err) {
            console.log('НЕ ПОКАЖУ ДИЕТУ ПОЛЬЗОВАТЕЛЯ: ' + err);
        }
    })();
});

module.exports = router;