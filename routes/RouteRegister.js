const indexRoute = require('./index');
const homeRoute = require('./home');
const loginRoute = require('./login');
const signoutRoute = require('./signout');
const signupRoute = require('./signup');


module.exports = function (app) {
    app.use('/', indexRoute);
    app.use('/', homeRoute);
    app.use('/', loginRoute);
    app.use('/', signoutRoute);
    app.use('/', signupRoute);
};