const express = require('express');
const router = express.Router();
const passport = require('passport');

/* Handle Login POST */
router.post('/login', passport.authenticate('login', {
    successRedirect: '/home',
    failureRedirect: '/',
    failureFlash: true
}));

module.exports = router;