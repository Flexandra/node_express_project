const express = require('express');
const router = express.Router();

/* Handle Logout */
router.get('/signout', function (req, res) {
    req.logout();
    res.redirect('/');
});

module.exports = router;